//
//  GameController.m
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "GameController.h"

@implementation GameController

+ (id) getGameController {
    
    static GameController * gameController;
    
    @synchronized (self) {
        if (gameController == nil)
            gameController = [[GameController alloc]init];
    }
    
    return gameController;
}

- (void) loadCategories {
	_m_categories = [QuestionDataManager loadCategories];
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        _m_user = [[GameUser alloc] init];
		[self loadCategories];
    }
	
    return self;
}

- (void) setGameUser:(GameUser *)p_user {
	_m_user = p_user;
}

- (void) setQuestions:(NSMutableArray *)p_questions {
	_m_questions = p_questions;
}

- (void) sortOrderOfQuestions {
	
	NSInteger randomInteger = arc4random() % _m_questions.count;
	_m_question_order = [[NSMutableArray alloc] init];
	
	while(_m_question_order.count < _m_questions.count) {
		if(![_m_question_order containsObject:[NSNumber numberWithInt:randomInteger]]) {
			[_m_question_order addObject:[NSNumber numberWithInt:randomInteger]];
		} else {
			randomInteger = arc4random()%_m_questions.count;
		}
	}
}

- (void) toogleSuccess:(BOOL)p_success {
	_success = p_success;
}

@end
