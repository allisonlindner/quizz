//
//  User.h
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDataManager.h"

@interface GameUser : NSObject

- (instancetype) initWithLogin:(NSString *)p_login Password:(NSString *)p_password Name:(NSString *)p_name;
- (instancetype) initWithLogin:(NSString *)p_login Password:(NSString *)p_password;

@property NSString * m_login;
@property NSString * m_password;
@property NSString * m_name;

@property int m_lastScore;
@property int m_highScore;
@property int vidas;

- (BOOL) verifyLogin;
- (void) setScore:(int)p_score;
- (void) addHealth:(int)p_health;
- (void) removeHealth;

@end
