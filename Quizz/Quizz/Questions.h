//
//  Questions.h
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Questions : NSObject

@property NSString * m_question;
@property NSMutableArray * m_answers;
@property NSString * m_rightAnswer;

- (BOOL)checkAnswer:(NSString *)p_answer;
- (instancetype)initWithQuestion:(NSString *)p_question Answers:(NSArray *)p_answers;

@end
