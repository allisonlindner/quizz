//
//  QuestionDataManager.h
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileManager.h"
#import "Questions.h"

@interface QuestionDataManager : NSObject

+ (NSMutableArray *)loadQuestionsWithCategory:(NSString *)p_category;
+ (NSMutableArray *)loadCategories;

@end
