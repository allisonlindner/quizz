//
//  GameController.h
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameUser.h"
#import "Questions.h"
#import "QuestionDataManager.h"

@interface GameController : NSObject

+ (id) getGameController;
- (void) loadCategories;
- (void) setGameUser:(GameUser *)p_user;
- (void) setQuestions:(NSMutableArray *)p_questions;
- (void) sortOrderOfQuestions;
- (void) toogleSuccess:(BOOL)p_success;
- (void) setLastCategory:(int)p_lastCategory;

@property GameUser * m_user;
@property NSMutableArray * m_questions;
@property NSMutableArray * m_categories;
@property NSMutableArray * m_question_order;
@property BOOL success;
@property int lastCategory;

@end
