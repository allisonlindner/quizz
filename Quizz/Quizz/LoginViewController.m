//
//  LoginViewController.m
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _login = @"";
    _password = @"";
    _name = @"";
	
	_keyUP = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerUser:(id)sender {
    
    GameUser * user;
    
    if(![_name isEqualToString:@""]) {
        
        _labelError.hidden = true;
    
        user = [[GameUser alloc]initWithLogin: _login Password:_password Name:_name];
        
        if ([UserDataManager registerUser:user]) {
            _labelError.highlighted = true;
            _labelError.text = @"Registrado com sucesso";
			_labelError.hidden = false;
			[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];

        } else {
            _labelError.highlighted = false;
            _labelError.text = @"Usuário já existe";
			_labelError.hidden = false;
			[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];
        }
        
        [[GameController getGameController] setGameUser:user];
            
    } else {
        _labelError.highlighted = false;
        _labelError.text = @"Preencha todos os campos";
		_labelError.hidden = false;
		[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];
    }
}

- (IBAction)loginUser:(id)sender {
    
    GameUser * user = [[GameUser alloc] initWithLogin:_login Password:_password];
    
    if([user verifyLogin]) {
        [[GameController getGameController] setGameUser:user];
		[[GameController getGameController] toogleSuccess:false];
        
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HomeViewController * homeViewController = (HomeViewController *)[storyboard instantiateViewControllerWithIdentifier:@"home"];
        [self presentViewController:homeViewController animated:YES completion:nil];
	} else {
		_labelError.highlighted = false;
		_labelError.text = @"Senha incorreta";
		_labelError.hidden = false;
		[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];
	}
}

#pragma mark UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if(!_keyUP) {
		if(textField.tag == 0){
			[UIView beginAnimations:nil context:NULL];
			[UIView setAnimationDelegate:self];
			[UIView setAnimationDuration:0.4];
			[UIView setAnimationBeginsFromCurrentState:YES];
			self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 200), self.view.frame.size.width, self.view.frame.size.height);
			[UIView commitAnimations];
		}
		
		if(textField.tag == 1){
			[UIView beginAnimations:nil context:NULL];
			[UIView setAnimationDelegate:self];
			[UIView setAnimationDuration:0.4];
			[UIView setAnimationBeginsFromCurrentState:YES];
			self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 200), self.view.frame.size.width, self.view.frame.size.height);
			[UIView commitAnimations];
		}
		
		if(textField.tag == 2){
			[UIView beginAnimations:nil context:NULL];
			[UIView setAnimationDelegate:self];
			[UIView setAnimationDuration:0.4];
			[UIView setAnimationBeginsFromCurrentState:YES];
			self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y - 200), self.view.frame.size.width, self.view.frame.size.height);
			[UIView commitAnimations];
		}
		
		_keyUP = true;
	}
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if (textField.tag == 0) {
        _login = textField.text;
    }
    
    if (textField.tag == 1) {
        _password = textField.text;
    }
    
    if (textField.tag == 2) {
        _name = textField.text;
    }
    
    
    if([_login isEqualToString:@""] || [_password isEqualToString:@""]) {
        _labelError.highlighted = false;
        _labelError.text = @"Preencha todos os campos";
		_labelError.hidden = false;
		[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];
    } else {
        _labelError.hidden = true;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if([_login isEqualToString:@""] || [_password isEqualToString:@""]) {
        _labelError.text = @"Preencha todos os campos";
        _labelError.hidden = false;
		[super performSelector:@selector(cleanError) withObject:@"Clean error!" afterDelay:1.5];
    } else {
        _labelError.hidden = true;
    }
	
	if(textField.tag == 0){
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDuration:0.2];
		[UIView setAnimationBeginsFromCurrentState:YES];
		self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 200), self.view.frame.size.width, self.view.frame.size.height);
		[UIView commitAnimations];
	}
	
	if(textField.tag == 1){
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDuration:0.2];
		[UIView setAnimationBeginsFromCurrentState:YES];
		self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 200), self.view.frame.size.width, self.view.frame.size.height);
		[UIView commitAnimations];
	}
	
	if(textField.tag == 2){
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDuration:0.2];
		[UIView setAnimationBeginsFromCurrentState:YES];
		self.view.frame = CGRectMake(self.view.frame.origin.x, (self.view.frame.origin.y + 200), self.view.frame.size.width, self.view.frame.size.height);
		[UIView commitAnimations];
	}
	
	_keyUP = false;
	
    return [textField resignFirstResponder];
}

- (void)cleanError {
	_labelError.text = @"";
	_labelError.hidden = true;
}

@end
