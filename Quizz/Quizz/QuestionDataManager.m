//
//  QuestionDataManager.m
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "QuestionDataManager.h"

@implementation QuestionDataManager

+ (NSMutableArray *)loadQuestionsWithCategory:(NSString *)p_category {
	
	NSMutableArray * questions = [[NSMutableArray alloc]init];
	
    NSString * categoryFilePath = [[NSString alloc] initWithFormat:@"%@_category.data", p_category];
	NSString * allQuestionString = [FileManager LoadStringFromFile:categoryFilePath useBundle:true];
	
	NSArray * allQuestions = [allQuestionString componentsSeparatedByString:@"\n\n"];
	
	for(int i = 0; i < allQuestions.count; i++) {
		NSArray * individualQuestion = [allQuestions[i] componentsSeparatedByString:@"\n"];
		NSString * questionTitle = individualQuestion[0];
		
		NSArray * answers = [individualQuestion[1] componentsSeparatedByString:@"|"];
		
		Questions * question = [[Questions alloc] initWithQuestion:questionTitle Answers:answers];
		
		[questions addObject:question];
	}
	
	return questions;
}

+ (NSMutableArray *)loadCategories {
	
	NSString * categoriesFileString = [FileManager LoadStringFromFile:@"categories.data" useBundle:YES];
	NSArray * categoriesWithImages = [categoriesFileString componentsSeparatedByString:@"\n"];
	NSMutableArray * categories = [[NSMutableArray alloc] init];
	
	for (int i = 0; i < categoriesWithImages.count; i++) {
		[categories insertObject:[categoriesWithImages[i] componentsSeparatedByString:@"|"] atIndex:i];
	}
	
	return categories;
}

@end