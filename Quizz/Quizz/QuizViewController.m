//
//  QuizViewController.m
//  Quizz
//
//  Created by Allison Lindner on 20/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "QuizViewController.h"

@interface QuizViewController ()

@end

@implementation QuizViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[[GameController getGameController] sortOrderOfQuestions];
	_current_question = 0;
	_timerCount = 0;
	
	[self resetButtons];
	[self randomAnswers];
	[self writeQuizOnView];
	
	[NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(calculateScoreOverTime) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)randomAnswers {
	
	_answers_order = [[NSMutableArray alloc] init];
	
	NSInteger randomInteger = arc4random() % 5;
	
	while(_answers_order.count < 5) {
		if(![_answers_order containsObject:[NSNumber numberWithInt:randomInteger]]) {
			[_answers_order addObject:[NSNumber numberWithInt:randomInteger]];
		} else {
			randomInteger = arc4random() % 5;
		}
	}
}

//Método para segurar por um tempo antes da proxima pergunta
- (void)beforeNextQuestion {
	[self nextQuestion];
	[self randomAnswers];
	[self writeQuizOnView];
}

- (void)nextQuestion {
	_timerCount = 0;
	
	if(_current_question < ([[GameController getGameController] m_questions].count - 1)) {
		_current_question++;
	} else {
		[[[GameController getGameController] m_user] setScore:_currentScore];
		[[GameController getGameController] toogleSuccess:true];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)wrongAnswer {
	if([[[GameController getGameController] m_user] vidas] > 0) {
		[[[GameController getGameController] m_user] removeHealth];
		[_imageHealth setAlpha:1.0];
		[super performSelector:@selector(removeAlphaHealth) withObject:@"Coração pulsar" afterDelay:0.2];
		[self nextQuestion];
		[self randomAnswers];
		[self writeQuizOnView];
	} else {
		[[GameController getGameController] toogleSuccess:true];
		[[[GameController getGameController] m_user] setScore:_currentScore];
		[self dismissViewControllerAnimated:YES completion:nil];
	}
}

- (void)resetButtons {
	_buttonImageA.hidden = true;
	_buttonImageB.hidden = true;
	_buttonImageC.hidden = true;
	_buttonImageD.hidden = true;
	_buttonImageE.hidden = true;
	[_buttonA setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65f]];
	[_buttonB setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65f]];
	[_buttonC setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65f]];
	[_buttonD setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65f]];
	[_buttonE setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.65f]];
	
	[self enableAllButtons];
}

- (void)desableAllButtons {
	_buttonA.enabled = false;
	_buttonB.enabled = false;
	_buttonC.enabled = false;
	_buttonD.enabled = false;
	_buttonE.enabled = false;
}

- (void)enableAllButtons {
	_buttonA.enabled = true;
	_buttonB.enabled = true;
	_buttonC.enabled = true;
	_buttonD.enabled = true;
	_buttonE.enabled = true;
}

- (void)writeScoreOnView {
	NSString * score = [[NSString alloc] initWithFormat:@"   Score: %i", _currentScore];
	[_labelScore setText:score];
	
	NSString * vidas = [[NSString alloc] initWithFormat:@"%i", [[[GameController getGameController] m_user] vidas]];
	[_labelHealth setText:vidas];
}

- (void)writeQuizOnView {
	[self resetButtons];
	[self writeScoreOnView];
	
	NSMutableArray * questions = [[GameController getGameController] m_questions];
	NSMutableArray * questions_order = [[GameController getGameController] m_question_order];
	
	Questions * question = [questions objectAtIndex:[[questions_order objectAtIndex:_current_question] integerValue]];
	[_labelQuestion setText:question.m_question];
	
	for(int answerIndex = 0; answerIndex < 5; answerIndex++) {
		if([[_answers_order objectAtIndex:answerIndex] integerValue] == 0) {
			[_buttonA setTitle:[question.m_answers objectAtIndex:answerIndex] forState:UIControlStateNormal];
			
		}
		if([[_answers_order objectAtIndex:answerIndex] integerValue] == 1) {
			[_buttonB setTitle:[question.m_answers objectAtIndex:answerIndex] forState:UIControlStateNormal];
			
		}
		if([[_answers_order objectAtIndex:answerIndex] integerValue] == 2) {
			[_buttonC setTitle:[question.m_answers objectAtIndex:answerIndex] forState:UIControlStateNormal];
			
		}
		if([[_answers_order objectAtIndex:answerIndex] integerValue] == 3) {
			[_buttonD setTitle:[question.m_answers objectAtIndex:answerIndex] forState:UIControlStateNormal];
			
		}
		if([[_answers_order objectAtIndex:answerIndex] integerValue] == 4) {
			[_buttonE setTitle:[question.m_answers objectAtIndex:answerIndex] forState:UIControlStateNormal];
		}
	}
}

- (IBAction)checkAnswer:(id)sender {
	[self resetButtons];
	
	[self desableAllButtons];
	
    UIButton * buttonPressed = (UIButton *)sender;
    
    NSMutableArray * questions = [[GameController getGameController] m_questions];
    NSMutableArray * questions_order = [[GameController getGameController] m_question_order];
    
    Questions * question = [questions objectAtIndex:[[questions_order objectAtIndex:_current_question] integerValue]];
	
	if([question checkAnswer:[[buttonPressed titleLabel] text]]) {
		[buttonPressed setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
		
		if(buttonPressed.tag == 0) {
			[_buttonImageA setImage:[UIImage imageNamed:@"icon_ok.png"]];
			_buttonImageA.hidden = false;
		} else if(buttonPressed.tag == 1) {
			[_buttonImageB setImage:[UIImage imageNamed:@"icon_ok.png"]];
			_buttonImageB.hidden = false;
		} else if(buttonPressed.tag == 2) {
			[_buttonImageC setImage:[UIImage imageNamed:@"icon_ok.png"]];
			_buttonImageC.hidden = false;
		} else if(buttonPressed.tag == 3) {
			[_buttonImageD setImage:[UIImage imageNamed:@"icon_ok.png"]];
			_buttonImageD.hidden = false;
		} else if(buttonPressed.tag == 4) {
			[_buttonImageE setImage:[UIImage imageNamed:@"icon_ok.png"]];
			_buttonImageE.hidden = false;
		}
		
		_currentScore += 50;
		[self performSelector:@selector(beforeNextQuestion) withObject:@"Delay para proxima resposta" afterDelay:1.0];
	} else {
		[buttonPressed setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.5f]];
		
		if(buttonPressed.tag == 0) {
			[_buttonImageA setImage:[UIImage imageNamed:@"icon_wrong.png"]];
			_buttonImageA.hidden = false;
		} else if(buttonPressed.tag == 1) {
			[_buttonImageB setImage:[UIImage imageNamed:@"icon_wrong.png"]];
			_buttonImageB.hidden = false;
		} else if(buttonPressed.tag == 2) {
			[_buttonImageC setImage:[UIImage imageNamed:@"icon_wrong.png"]];
			_buttonImageC.hidden = false;
		} else if(buttonPressed.tag == 3) {
			[_buttonImageD setImage:[UIImage imageNamed:@"icon_wrong.png"]];
			_buttonImageD.hidden = false;
		} else if(buttonPressed.tag == 4) {
			[_buttonImageE setImage:[UIImage imageNamed:@"icon_wrong.png"]];
			_buttonImageE.hidden = false;
		}
		
		for(int i = 0; i < 5; i++) {
			if(i == 0) {
				if([question checkAnswer:[[_buttonA titleLabel] text]]) {
					[_buttonA setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
					[_buttonImageA setImage:[UIImage imageNamed:@"icon_ok.png"]];
					_buttonImageA.hidden = false;
				} else if([question checkAnswer:[[_buttonB titleLabel] text]]) {
					[_buttonB setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
					[_buttonImageB setImage:[UIImage imageNamed:@"icon_ok.png"]];
					_buttonImageB.hidden = false;
				} else if([question checkAnswer:[[_buttonC titleLabel] text]]) {
					[_buttonC setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
					[_buttonImageC setImage:[UIImage imageNamed:@"icon_ok.png"]];
					_buttonImageC.hidden = false;
				} else if([question checkAnswer:[[_buttonD titleLabel] text]]) {
					[_buttonD setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
					[_buttonImageD setImage:[UIImage imageNamed:@"icon_ok.png"]];
					_buttonImageD.hidden = false;
				} else if([question checkAnswer:[[_buttonE titleLabel] text]]) {
					[_buttonE setBackgroundColor:[[UIColor greenColor] colorWithAlphaComponent:0.5f]];
					[_buttonImageE setImage:[UIImage imageNamed:@"icon_ok.png"]];
					_buttonImageE.hidden = false;
				}
			}
		}
		
		if(_currentScore >= 10) {
			_currentScore -= 10;
		} else {
			_currentScore = 0;
		}
		
		[self performSelector:@selector(wrongAnswer) withObject:@"Delay para proxima resposta" afterDelay:1.0];
    }
}

#pragma mark Contador de Pontos por Tempo
	
- (void)calculateScoreOverTime {
	_timerCount++;
	
	if(_currentScore > 0) {
		_currentScore --;
		_labelScore.highlighted = true;
	} else
		_currentScore = 0;
	
	[self performSelector:@selector(removeHighlighted) withObject:@"Score piscando" afterDelay:0.1];
	[self writeScoreOnView];
}

- (void) removeHighlighted {
	_labelScore.highlighted = false;
}

- (void) removeAlphaHealth {
	[_imageHealth setAlpha:0.5];
}

@end
