//
//  User.m
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "GameUser.h"

@implementation GameUser

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        _m_login = @"";
        _m_password = @"";
        _m_name = @"";
        _m_lastScore = 0;
        _m_highScore = 0;
		_vidas = 3;
    }
    
    return self;
}

- (instancetype) initWithLogin:(NSString *)p_login Password:(NSString *)p_password {
    
    self = [super init];
    
    if (self) {
        _m_login = p_login;
        _m_password = p_password;
        
        _m_name = @"";
        _m_lastScore = 0;
		_m_highScore = 0;
		_vidas = 3;
    }
    
    return self;
}

- (instancetype) initWithLogin:(NSString *)p_login Password:(NSString *)p_password Name:(NSString *)p_name {
    
    self = [super init];
    
    if (self) {
        _m_login = p_login;
        _m_password = p_password;
        _m_name = p_name;
        
        _m_lastScore = 0;
		_m_highScore = 0;
		_vidas = 3;
    }
    
    return self;
}

- (BOOL) verifyLogin {
	
	GameUser * user;
	
	user = [UserDataManager loadUserWithLogin:_m_login];
    
    if ([user.m_login isEqualToString:_m_login] && [user.m_password isEqualToString:_m_password]) {
		_m_name = user.m_name;
        [UserDataManager loadScoresForUser:self];
        return YES;
    } else {
        return NO;
    }
}

- (void) setScore:(int)p_score {
	_m_lastScore = p_score;
	
	if(_m_lastScore > _m_highScore)
		_m_highScore = _m_lastScore;
}

- (void) addHealth:(int)p_health {
	_vidas = p_health;
}

- (void) removeHealth {
	if(_vidas > 0)
		_vidas--;
}

@end
