//
//  QuizViewController.h
//  Quizz
//
//  Created by Allison Lindner on 20/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameController.h"
#import "Questions.h"

@interface QuizViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *labelQuestion;
@property (weak, nonatomic) IBOutlet UIButton *buttonA;
@property (weak, nonatomic) IBOutlet UIButton *buttonB;
@property (weak, nonatomic) IBOutlet UIButton *buttonC;
@property (weak, nonatomic) IBOutlet UIButton *buttonD;
@property (weak, nonatomic) IBOutlet UIButton *buttonE;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageA;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageB;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageC;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageD;
@property (weak, nonatomic) IBOutlet UIImageView *buttonImageE;
@property (weak, nonatomic) IBOutlet UILabel *labelScore;
@property (weak, nonatomic) IBOutlet UILabel *labelHealth;
@property (weak, nonatomic) IBOutlet UIImageView *imageHealth;

@property NSMutableArray * answers_order;
@property int current_question;
@property int currentScore;
@property int timerCount;

- (void)randomAnswers;
- (void)writeScoreOnView;
- (void)writeQuizOnView;
- (void)beforeNextQuestion;
- (void)nextQuestion;
- (void)wrongAnswer;
- (void)resetButtons;
- (IBAction)checkAnswer:(id)sender;
- (void)calculateScoreOverTime;
- (void)removeHighlighted;

@end
