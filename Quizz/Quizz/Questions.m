//
//  Questions.m
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "Questions.h"

@implementation Questions

- (instancetype)initWithQuestion:(NSString *)p_question Answers:(NSArray *)p_answers {
	self = [super init];
	
	_m_question = p_question;
	_m_answers = [[NSMutableArray alloc] init];
	
	for (int i = 0; i < (p_answers.count-1); i++) {
		[_m_answers addObject:p_answers[i]];
	}
	
	int rightAnswer = (int) [p_answers[p_answers.count-1] integerValue];
	_m_rightAnswer = p_answers[rightAnswer-1];
	
	return self;
}

- (BOOL)checkAnswer:(NSString *)p_answer {
	
	if([p_answer isEqualToString:_m_rightAnswer])
		return YES;
	
	return NO;
}

@end
