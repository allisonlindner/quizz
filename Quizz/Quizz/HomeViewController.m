//
//  HomeViewController.m
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "HomeViewController.h"
#import "QuizViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"I'm here, next view.");
	
	[[GameController getGameController] loadCategories];
	GameUser * gameUser = [[GameController getGameController] m_user];
	
	_labelUserName.text = gameUser.m_name;
	_buttonPlay.enabled = true;
	[[_buttonPlay titleLabel] setTextColor:[UIColor blackColor]];
	[_buttonPlay setTitle:@"" forState:UIControlStateDisabled];
}

- (void)viewWillAppear:(BOOL)animated {
	NSLog(@"I'm here again.");
	[self showScoreOnView];
	
	if([[GameController getGameController] success]) {
		[[[GameController getGameController] m_categories] removeObjectAtIndex:([[GameController getGameController] lastCategory] - 1)];
		[_categoryPickerView reloadAllComponents];
		[[[GameController getGameController] m_user] addHealth:3];
	}
	
	if([[[GameController getGameController] m_categories] count] == 0) {
		[_imageCategory setImage:[UIImage imageNamed:@"end.png"]];
	}
}

- (void) viewDidAppear:(BOOL)animated {
	int row = (int)[[[GameController getGameController] m_categories] count];
	
	if(row > 1) {
		[self pickerView:_categoryPickerView didSelectRow:row/2 inComponent:0];
		[_categoryPickerView selectRow:row/2 inComponent:0 animated:YES];
	} else {
		[self pickerView:_categoryPickerView didSelectRow:0 inComponent:0];
		[_categoryPickerView selectRow:0 inComponent:0 animated:YES];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showScoreOnView {
	GameUser * gameUser = [[GameController getGameController] m_user];
	
	[UserDataManager saveScoresForUser:gameUser];
	
	NSString * score = [[NSString alloc] initWithFormat:@"Last Score: %i", gameUser.m_lastScore];
	[_labelLastScore setText:score];
	
	score = [[NSString alloc] initWithFormat:@"High Score: %i", gameUser.m_highScore];
	[_labelHighScore setText:score];
}

- (IBAction)closeView:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	GameController * control;
	control = [GameController getGameController];
	
	if([[segue identifier] isEqualToString:@"jogar"]) {
		control.lastCategory = ((int)[_categoryPickerView selectedRowInComponent:0]) + 1;
		
		QuizViewController * qvc = [segue destinationViewController];
		
		if([[GameController getGameController] success]) {
			qvc.currentScore = [[[GameController getGameController] m_user] m_lastScore];
			[[GameController getGameController] toogleSuccess:false];
		} else {
			qvc.currentScore = 0;
			[[GameController getGameController] toogleSuccess:false];
		}
	}
}

#pragma mark UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	if([[GameController getGameController] m_categories].count > 0)
		return [[GameController getGameController] m_categories].count;
	else
		return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if([[GameController getGameController] m_categories].count == 0) {
		_buttonPlay.enabled = false;
		return @"Fim :(";
	} else
		return [[[GameController getGameController] m_categories] objectAtIndex:row][component];
}

#pragma mark UIPickerViewDataSource

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
	if([[GameController getGameController] m_categories].count != 0) {
		NSString * category;
		NSString * categoryImage;
		categoryImage = [[[GameController getGameController] m_categories] objectAtIndex:row][2];
		category = [[[GameController getGameController] m_categories] objectAtIndex:row][1];
		[_imageCategory setImage:[UIImage imageNamed:categoryImage]];
		[[GameController getGameController] setQuestions: [QuestionDataManager loadQuestionsWithCategory:category]];
	}
}

@end
