//
//  HomeViewController.h
//  Quizz
//
//  Created by Allison Lindner on 19/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameController.h"
#import "QuestionDataManager.h"
#import "GameUser.h"

@interface HomeViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelUserName;
@property (weak, nonatomic) IBOutlet UILabel *labelLastScore;
@property (weak, nonatomic) IBOutlet UILabel *labelHighScore;
@property (weak, nonatomic) IBOutlet UIImageView *imageCategory;
@property (weak, nonatomic) IBOutlet UIPickerView *categoryPickerView;
@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;

- (void)showScoreOnView;
- (IBAction)closeView:(id)sender;

@end
