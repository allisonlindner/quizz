//
//  LoginViewController.h
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameUser.h"
#import "GameController.h"
#import "UserDataManager.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *buttonRegister;
@property (weak, nonatomic) IBOutlet UIButton *buttonLogin;
@property (weak, nonatomic) IBOutlet UILabel *labelError;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;

@property NSString * login;
@property NSString * password;
@property NSString * name;

- (IBAction)registerUser:(id)sender;
- (IBAction)loginUser:(id)sender;
- (void)cleanError;

@property BOOL keyUP;

@end
