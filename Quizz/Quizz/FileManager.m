//
//  FileManager.m
//  ManupulandoArquivos
//
//  Created by Mark Joselli on 3/13/15.
//  Copyright (c) 2015 Mark Joselli. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+(NSString *)LoadStringFromFile:(NSString *)file useBundle:(BOOL)bundle{
    
    
    NSString *documentPath;
    if(bundle){
        documentPath = [[NSBundle mainBundle].bundlePath stringByAppendingPathComponent:file];
    }else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        documentPath = [[paths objectAtIndex:0]stringByAppendingPathComponent:file];
        //NSLog(documentPath);
    }
    
    NSError *error;
    
    NSString *contentOfFile = [[NSString alloc] initWithContentsOfFile:documentPath encoding:NSUTF8StringEncoding error:&error];
    
    if(contentOfFile == nil){
        NSLog(@"Erro reading file:%@",[error description]);
    }
    
    return contentOfFile;

}

+(BOOL)SaveString:(NSString *)string inFile:(NSString *)file{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentPath;
    
    documentPath = [[paths objectAtIndex:0]stringByAppendingPathComponent:file];
    
    NSError *error;
    
    BOOL sucess = [string writeToFile:documentPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if(!sucess){
        NSLog(@"Erro writing file:%@",[error description]);
    }
    
    return sucess;

}

+(BOOL)RemoveFileAndSaveString:(NSString *)string inFile:(NSString *)file{
	NSFileManager *filemgr;
	
	filemgr = [NSFileManager defaultManager];
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	NSString *documentPath;
	
	documentPath = [[paths objectAtIndex:0]stringByAppendingPathComponent:file];
	
	if ([filemgr removeItemAtPath:documentPath error: NULL]  == YES)
		NSLog (@"Remove successful");
	else
		NSLog (@"Remove failed");
	
	NSError *error;
	
	BOOL sucess = [string writeToFile:documentPath atomically:YES encoding:NSUTF8StringEncoding error:&error];
	if(!sucess){
		NSLog(@"Erro writing file:%@",[error description]);
	}
	
	return sucess;
	
}

@end
