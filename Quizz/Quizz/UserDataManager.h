//
//  UserDataManager.h
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileManager.h"

@class GameUser;
@interface UserDataManager : NSObject

+ (GameUser *) loadUserWithLogin:(NSString *)p_login;
+ (BOOL) registerUser:(GameUser *)p_user;
+ (void) loadScoresForUser:(GameUser *)p_user;
+ (void) saveScoresForUser:(GameUser *)p_user;

@end
