//
//  UserDataManager.m
//  Quizz
//
//  Created by Allison Lindner on 18/03/15.
//  Copyright (c) 2015 Allison Lindner. All rights reserved.
//

#import "UserDataManager.h"
#import "GameUser.h"

@implementation UserDataManager

+ (GameUser *) loadUserWithLogin:(NSString *)p_login {
    
    GameUser * user;
    
    NSString * userFilePath = [[NSString alloc]initWithFormat:@"%@_login.data", p_login];
    NSString * userFileString = [FileManager LoadStringFromFile:userFilePath useBundle:NO];
    
    if(userFileString != nil) {
        
        NSArray * userInfo = [userFileString componentsSeparatedByString:@";"];
        user = [[GameUser alloc] initWithLogin:userInfo[0] Password:userInfo[1] Name:userInfo[2]];
        [self loadScoresForUser:user];
        
        return user;
    }
    
    return nil;
}

+ (BOOL) registerUser:(GameUser *)p_user {
    
    NSString * userInfo = [[NSString alloc]initWithFormat:@"%@;%@;%@", p_user.m_login, p_user.m_password, [p_user.m_name capitalizedString]];
    NSString * userFilePath = [[NSString alloc]initWithFormat:@"%@_login.data", p_user.m_login];
    
    if([FileManager LoadStringFromFile:userFilePath useBundle:NO] == nil) {
        [FileManager SaveString:userInfo inFile:userFilePath];
        [self saveScoresForUser:p_user];
        return YES;
    } else {
        
        return NO;
    }
}

+ (void) loadScoresForUser:(GameUser *)p_user {
    
    NSArray * scores;
    
    NSString * scoreFilePath = [[NSString alloc]initWithFormat:@"%@_scores.data", p_user.m_login];
    NSString * scoreFileString = [FileManager LoadStringFromFile:scoreFilePath useBundle:NO];
    
    if (scoreFileString == nil) {
        p_user.m_lastScore = 0;
        p_user.m_highScore = 0;
        
        scoreFileString = @"0;0";
        [FileManager SaveString:scoreFileString inFile:scoreFilePath];
    } else {
        scores = [scoreFileString componentsSeparatedByString:@";"];
        p_user.m_lastScore = (int)[scores[0] integerValue];
        p_user.m_highScore = (int)[scores[1] integerValue];
    }
}

+ (void) saveScoresForUser:(GameUser *)p_user {
    
    NSString * scoreFileString;
    
    NSString * scoreFilePath = [[NSString alloc]initWithFormat:@"%@_scores.data", p_user.m_login];
    
    scoreFileString = [[NSString alloc]initWithFormat:@"%i;%i", p_user.m_lastScore, p_user.m_highScore];
    [FileManager RemoveFileAndSaveString:scoreFileString inFile:scoreFilePath];
}

@end
